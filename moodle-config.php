<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'db';
$CFG->dbname    = 'moodle';
$CFG->dbuser    = 'moodle';
$CFG->dbpass    = 'moodle';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);

$CFG->phpunit_dbtype    = 'pgsql';
$CFG->phpunit_dblibrary = 'native';
$CFG->phpunit_dbhost    = 'test-db';
$CFG->phpunit_dbname    = 'moodle';
$CFG->phpunit_dbuser    = 'moodle';
$CFG->phpunit_dbpass    = 'moodle';
$CFG->phpunit_prefix    = 'phpu_';
$CFG->phpunit_dataroot = '/var/lib/testsitedata';

$CFG->wwwroot   = 'http://127.0.0.1:8080';
$CFG->dataroot  = '/data';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

if (true) {
    // Force a debugging mode regardless the settings in the site administration
    @error_reporting(E_ALL | E_STRICT); // NOT FOR PRODUCTION SERVERS!
    @ini_set('display_errors', '1');    // NOT FOR PRODUCTION SERVERS!
    $CFG->debug = (E_ALL | E_STRICT);   // === DEBUG_DEVELOPER - NOT FOR PRODUCTION SERVERS!
    $CFG->debugdisplay = 1;             // NOT FOR PRODUCTION SERVERS!
    // Allow any type of password
    $CFG->passwordpolicy = false;
    $CFG->cachejs = false;
}

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
